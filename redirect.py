import socket
import random


# Lista de URLs
urls = ['https://www.example.com/', 'https://www.google.com/', 'https://www.github.com/', 'https://cursosweb.github.io/',
        'https://www.youtube.com/', 'https://www.aulavirtual.urjc.es/']

# Creamos un objeto socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Asociamos el socket a una direccion y puerto
server_socket.bind(('localhost', 8081))

# Escuchamos las solicitudes entrantes, 1 en este caso
server_socket.listen(1)

print('Servidor listo para recibir conexiones en el puerto...')

try:
    while True:
        # Aceptar una conexion entrante
        client_socket, client_address = server_socket.accept()
        print('Cliente conectado desde', client_address)

        # Recibir la solicitud HTTP del cliente
        request = client_socket.recv(2048).decode('utf-8')
        print(request)

        # Elegimos una url aleatoria de la lista
        url = random.choice(urls)

        # Crear la respuesta HTTP de redireccion
        response = 'HTTP/1.1 302 Found\r\nLocation: {}\r\n\r\n'.format(url)

        # Enviar la respuesta al cliente
        client_socket.sendall(response.encode('utf-8'))

        # Cerramos la conexion con el cliente
        client_socket.close()
except KeyboardInterrupt:
    print("Closing binded socket")
    server_socket.close()